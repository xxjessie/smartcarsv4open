from django.shortcuts import render
#from django.contrib.gis.geos import *
#from django.contrib.gis.geos import Point
#from django.contrib.gis.measure import D
#from django.contrib.gis.geos import fromstr
from django.core import serializers
import json
import redis
from django.http import HttpResponse
from clientapp.models import CarIdentity

# Create your views here.
def index(request):
    return render(request,'clientapp/index.html')

def request(request):
    sucess = False
    to_return = {'msg':'none'}
    r = redis.StrictRedis(host='smartcar.ef7p45.0001.use1.cache.amazonaws.com', port=6379, db=0)
    if request.method == "POST":
        post = request.POST.copy()
    if post.has_key('name'):
        name = post['name']
        new_region = post['new_region']        
        cars = CarIdentity.objects.filter(name = name)
        if cars.count() > 1:
            raise Exception("MORE THAN ONE CAR WITH NAME + " + name)
        if cars.count()>0:
            # carLoc = CarLoc.objects.filter(car_identity = cars[0].id)
            # to_return = {'msg':'duplicate car', 'id': cars[0].id, 'carObject': serializers.serialize("json", carLoc)}
            # serialized = json.dumps(to_return)

            #start redis
            if new_region == "1":
                
                #r = redis.StrictRedis(host='localhost', port=6379, db=0)
                r.hset(post['region_name'],cars[0].id,post['now_lat']+"_"+post['now_lng']+"_"+post['now_lat']
                    +"_"+post['now_lng']+"_"+"0")   
            to_return = {'msg':'duplicate car', 'id': cars[0].id}
            serialized = json.dumps(to_return)
            #end redis

            print serialized
        else:
            new_car = CarIdentity(name = name)
            new_car.save()
            # new_point = Point(-73.935,40.73)
            # new_loc = new_car.carloc_set.create(prev_lat = 100,prev_lng = 100,speed = 10,location = new_point)
            # new_loc.save()
            # to_return = {'msg': "new id provided = " + str(new_car.id), 'id': new_car.id}
            # serialized = json.dumps(to_return)

            #start redis
            r.hset(post['region_name'],str(new_car.id),post['now_lat']+"_"+post['now_lng']+"_"+post['now_lat']+"_"+post['now_lng']+"_"+"0")      
            to_return = {'msg':'new id provided', 'id': str(new_car.id)}
            serialized = json.dumps(to_return)
            #end redis


    return HttpResponse(serialized,content_type = "application/json")

def update(request):
    sucess = False
    if request.method == "POST":
        print "update called"
        post = request.POST.copy()
        if post.has_key('id'):
            id = post['id']
            print id
            if CarIdentity.objects.filter(id = id).count() > 0:
                # temp = CarIdentity.objects.get(id = id)
                # loc = temp.carloc_set.all()[0]
                # loc.prev_lat = post['prev_lat']
                # loc.prev_lng = post['prev_lng']
                # loc.speed = post['speed']
                # lat=post['now_lng']
                # lng=post['now_lat']
                # lat_lng=post['now_lng']+" "+post['now_lat']
                # point = fromstr('POINT('+lat_lng+')')
                # loc.location = point
                # loc.save()
                # qs = CarLoc.objects.all()
                # qs = CarLoc.objects.filter(location__distance_gte=(point,D(km=0.001)))
                # print "found qs"
                # if len(qs) == 0:
                #     print "no near cars"
                # else:
                #     print "length of qs = " + str(len(qs))
                # print qs

                
                # print "location updated"
                # data = serializers.serialize("json",qs)
                # to_return = {'msg':'id removed'}

                #start redis
                print "new_region is " 
                new_region = post['new_region']
                print new_region
                r = redis.StrictRedis(host='smartcar.ef7p45.0001.use1.cache.amazonaws.com', port=6379, db=0)
                #r = redis.StrictRedis(host='localhost', port=6379, db=0)
                
                if new_region == "1":
                    r.hdel(post['previous_region_name'],post['id'])
                    print "after delete"
                    r.hset(post['current_region_name'],post['id'],post['now_lat']+"_"+post['now_lng']+"_"+post['prev_lat']
                        +"_"+post['prev_lng']+ "_"+post['speed'])
                    print "after set"
                else:   
                    r.hset(post['current_region_name'],post['id'],post['now_lat']+"_"+post['now_lng']+"_"+post['prev_lat']
                        +"_"+post['prev_lng']+ "_"+post['speed'])
                # parse region_name and get the 8 closest
                region_lat = post['current_region_name'].split("_")[0]
                region_lng = post['current_region_name'].split("_")[1]
                print "after split"
                close_regions = []
                for i in range(-1,2):
                    for j in range (-1,2):
                        rnlat = int(region_lat)+i
                        rnlng = int(region_lng)+j
                        rn = str(rnlat)+"_"+str(rnlng)
                        close_regions.append(rn)
                close_cars = []
                for i in close_regions:
                    temp = r.hgetall(i)
                    if len(temp)!= 0:
                        close_cars.append(r.hgetall(i))

                for i in close_cars:
                    print i

                return_cars = {'carObject': json.dumps(close_cars)}
                data_redis = json.dumps(return_cars)
                print data_redis              
                #end redis


            else:
                to_return = {'msg':'supplied id does not exist in database'}
        else:
            to_return = {'msg':'request does not have id'}

 #   print to_return
    return HttpResponse(data_redis,content_type = "application/json")

def end(request):
    sucess = False
    to_return = {'msg':'none'}
    if request.method == "POST":
        post = request.POST.copy()
        if post.has_key('id'):
            id = post['id']
            print id
            if CarIdentity.objects.filter(id = id).count() > 0:
                temp = CarIdentity.objects.get(id = id)
                temp.delete()
                to_return = {'msg':'id removed'}
            else:
                to_return = {'msg':'supplied id does not exist in database'}
            #delete from redis    
            region = post['region_name']    
            #r = redis.StrictRedis(host='localhost', port=6379, db=0)
            r = redis.StrictRedis(host='smartcar.ef7p45.0001.use1.cache.amazonaws.com', port=6379, db=0)
            r.hdel(post['region_name'],post['id'])
        else:
            to_return = {'msg':'request does not have id'}
    serialized = json.dumps(to_return)
    print to_return
    return HttpResponse(serialized,content_type = "application/json")

