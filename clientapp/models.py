#from django.contrib.gis.db import models
#from django.contrib.gis.geos import Point
from django.db import models

class CarIdentity(models.Model):
    name = models.CharField(max_length=200)
    def __unicode__(self):
        return self.name

class CarLoc(models.Model):
    car_identity = models.ForeignKey(CarIdentity)
    prev_lat = models.FloatField(default =200)
    prev_lng = models.FloatField(default =200)
    speed = models.FloatField(default =0)
#    location = models.PointField()
#    objects = models.GeoManager()
