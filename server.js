var app=require('http').createServer()
var io = require('socket.io').listen(app)
var fs = require('fs')
var redis = require("redis")
app.listen(4000);
var redisClient = redis.createClient(6379,'chatserverv1.ef7p45.0001.use1.cache.amazonaws.com',{no_ready_check:true});
var redisPublishClient = redis.createClient(6379,'chatserverv1.ef7p45.0001.use1.cache.amazonaws.com',{no_ready_check:true});
var myChannel;
//look for connection errors and log
redisClient.on("error", function (err) {
    console.log("error event - " + redisClient.host + ":" + redisClient.port + " - " + err);
});


io.set('log level', 3);

   
io.sockets.on('connection', function (socket) {
//on connect send a welcome message
    socket.emit('message', { text : 'Welcome to chat!' });
    socket.on('subscribe', function (data) {
        socket.join(data.channel);
        myChannel = data.channel;
        redisClient.subscribe(myChannel);
    });
    socket.on('newMessage', function (data) {
        //io.sockets.in(data.channel).emit('message', {text:data.text});
        redisPublishClient.publish(data.channel,data.text);
        console.log("here")
    });
});


redisClient.on('ready', function() {
    redisClient.subscribe('systemNotifications');
});

      
redisClient.on("message", function(channel, message){
    var resp = {'text': message, 'channel':channel}
    io.sockets.in(channel).emit('message', resp);
});


