from django.conf.urls import patterns, include, url
from django.http import HttpResponse
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'smartCars.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^clientapp/',include('clientapp.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url (r'^robots\.txt$', lambda r: HttpResponse("User-agent: *\nDisallow: ", content_type="text/plain")),
)
