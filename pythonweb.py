# import webbrowser
# new = 2 
# url = "http://www.google.com"
# webbrowser.get('firefox').open_new_tab('http://127.0.0.1:8000/clientapp/')
from splinter import Browser  
from time import sleep                
with Browser() as browser:
	url = "http://127.0.0.1:8000/clientapp/"
	browser.visit(url)
	prompt = browser.get_alert()
	prompt.fill_with('car3')
	prompt.accept()
	browser.find_link_by_text('Request').click()
	prompt = browser.get_alert()
	prompt.accept()
	prompt = browser.get_alert()
	prompt.accept()
	browser.find_link_by_text('Select Points').click()
	dist = 0.001;
	start_lat = 40.7605;
	start_lng = -73.980;
	for i in range(0,5):
		lat = start_lat + i*dist;
		lng = start_lng + i*dist;	
		browser.find_by_id('demo_lat').fill(str(lat))
		browser.find_by_id('demo_lng').fill(str(lng))
		browser.find_by_tag('Button').click()
	browser.find_link_by_text('Stop').click()
	browser.find_link_by_text('Start').click()


	sleep(3000)
