from splinter import Browser  
from time import sleep  
import sys 
import random 

browser = Browser('phantomjs')
url = "http://smartcarsv3-1104377443.us-east-1.elb.amazonaws.com/clientapp/"
# url = "http://smartcarsv2-1731029048.us-east-1.elb.amazonaws.com/clientapp/"

# url = "http://ec2-54-85-37-94.compute-1.amazonaws.com/clientapp/"
#url = "http://ec2-54-84-76-93.compute-1.amazonaws.com/clientapp/"
browser.visit(url)
carid = int(sys.argv[1])
name = "car" + str(carid)
browser.find_by_id('demo_name').fill(name)
browser.find_by_tag('Button')[1].click()
browser.find_link_by_text('Request').click()
browser.find_link_by_text('Select Points').click()
dist = 0.001;
#start_lat = 40.7607;
#start_lng = -73.980;
lat = float(sys.argv[2]);
lng = float(sys.argv[3]);

# for i in range(0,5):
# 	lat = start_lat + i*dist;
# 	lng = start_lng + i*dist;	
browser.find_by_id('demo_lat').fill(str(lat))
browser.find_by_id('demo_lng').fill(str(lng))
browser.find_by_tag('Button').click()

browser.find_link_by_text('Stop').click()
browser.find_link_by_text('Start').click()
print "starting updates on "+name
count = 0 
while True:
	count+=1
	if count%100 == 0:
		print "made "+ str(count) + " updates on "+name

	time = random.randint(50000, 150000)
	sleep(float(time)/100000)
	# sleep(0.5)
	browser.find_by_id('request')[0].click() 

sleep(3000)
print "Done"
