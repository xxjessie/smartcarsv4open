from splinter import Browser  
from time import sleep  
import sys, getopt



def main(argv):

	numcars = 6 
	start = 0     

	lat_lngs = {"car1": [(40.758489, -73.975129),(40.759172, -73.976792), (40.760537, -73.979936), (40.761683, -73.982854), (40.758571, -73.985085)], 
	"car2" : [(40.761838, -73.983111), (40.760537, -73.979936), (40.759188, -73.976728), (40.762967, -73.973938)],
	"car3":[(40.764113, -73.973112),(40.758587, -73.977178), (40.762277, -73.986105)],
	"car4":[(40.764227, -73.981009), (40.763049, -73.978155), (40.761212, -73.979560),(40.759804, -73.976321)],
	"car5":[(40.759804, -73.976321), (40.761212, -73.979560),(40.763049, -73.978155),(40.764227, -73.981009) ],#reversed car4
	"car6":[(40.762277, -73.986105),(40.758587, -73.977178),(40.764113, -73.973112) ]}#reversed car 3


	browsers = []

	try:
	    opts, args = getopt.getopt(argv,"ns", ["num=", "start="])
	except getopt.GetoptError:
	    print 'proj.py -n [numCars] -s start'
	for opt, arg in opts:
	    if opt in ("-n", "--num"):
	        numcars = int(arg)
	    if opt in ("-s", "--start"):
	        start = int(arg)


	end = numcars
	if numcars < 4:
		end = numcars
	# else:
	# 	end = start + 4
	for i in range(start, end):
		name  = "car" + str(i + 1)
		dist = 0.001
		# url = "http://ec2-54-84-76-93.compute-1.amazonaws.com/clientapp/"
		# url = "http://smartcarsv2-1731029048.us-east-1.elb.amazonaws.com/clientapp/" #working
		url = "http://ec2-54-86-85-96.compute-1.amazonaws.com/clientapp/" #working 2
		# url ="http://ec2-54-85-37-94.compute-1.amazonaws.com/clientapp/"
		# url = "http://127.0.0.1:8000/clientapp/"
		# url = "http://smartcarsredis-521963118.us-east-1.elb.amazonaws.com/clientapp/"
		if i >20:
			browser = Browser('phantomjs')

			
			browser.visit(url)
			browser.find_by_id('demo_name').fill(name)
			browser.find_by_tag('Button')[1].click()
			browser.find_link_by_text('Request').click()
			browser.find_link_by_text('Select Points').click()
		else: 
			browser = Browser()
			browser.visit(url)
			# prompt0 = browser.get_alert()
			# prompt0.accept()

			prompt = browser.get_alert()
			prompt.fill_with(name)
			prompt.accept()
			browser.find_link_by_text('Request').click()
			prompt = browser.get_alert()
			prompt.accept()
			prompt = browser.get_alert()
			prompt.accept()
			browser.find_link_by_text('Select Points').click()

		browsers.append(browser)
		end_lat = 40.7607;
		end_lng = -73.980;
		for (lat,lng) in lat_lngs[name]:
			browser.find_by_id('demo_lat').fill(str(lat))
			browser.find_by_id('demo_lng').fill(str(lng))
			browser.find_by_tag('Button').click()
			end_lng = lng
			end_lat = lat
		for j in range(0,5):
			lat = end_lat + j*dist;
			lng = end_lng + j*dist;
			browser.find_by_id('demo_lat').fill(str(lat))
			browser.find_by_id('demo_lng').fill(str(lng))
			browser.find_by_tag('Button').click()
		print name + " ready"

	for browser in browsers:
		browser.find_link_by_text('Stop').click()
		browser.find_link_by_text('Start').click()


	print "started all cars"

	# #start_lat = 40.7607;
	# #start_lng = -73.980;
	# start_lat = float(sys.argv[2]);
	# start_lng = float(sys.argv[3]);


	sleep(3000)
	print "Done"


if __name__ == "__main__":
   main(sys.argv[1:])
