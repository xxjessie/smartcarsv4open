#! /bin/bash
apt-get -y update
apt-get -y upgrade
apt-get -y install apache2 libapache2-mod-wsgi
apt-get -y install python-pip
pip install django
apt-get -y install build-essential
apt-get -y install postgresql postgresql-client postgresql-contrib python-pygresql
apt-get -y install postgresql-server-dev-all
apt-get -y install python2.7-dev
easy_install psycopg2
apt-get -y install git
pip install redis
cd /home/ubuntu
git clone https://xxjessie@bitbucket.org/xxjessie/smartcarsv4open.git 
sudo apt-get -y install binutils libproj-dev gdal-bin
#wget http://download.osgeo.org/geos/geos-3.3.8.tar.bz2
#tar xjf geos-3.3.8.tar.bz2
#cd geos-3.3.8
#./configure
#make
#sudo make install
#cd ..

echo WSGIScriptAlias / /home/ubuntu/smartcarsv4open/smartCars/wsgi.py > httpd.conf
echo WSGIPythonPath /home/ubuntu/smartcarsv4open >> httpd.conf

echo "<Directory /home/ubuntu/smartcarsv4open/smartCars>" >> httpd.conf
echo "<Files wsgi.py>" >> httpd.conf
echo "Order deny,allow" >> httpd.conf
echo "Allow from all" >> httpd.conf
echo "</Files>" >> httpd.conf
echo "</Directory>" >> httpd.conf
sudo mv httpd.conf /etc/apache2/httpd.conf
sudo service apache2 restart

sudo apt-get install -y python-software-properties python g++ 
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo apt-get -y install nodejs
sudo apt-get -y install npm
sudo npm config set registry http://registry.npmjs.org/
sudo npm install socket.io
sudo npm install redis
cd smartcarsv4open
cd clientapp/templates/clientapp
pubip=$(curl http://169.254.169.254/latest/meta-data/public-hostname)
sudo sed -i "27i<script src="http://$pubip:4000/socket.io/socket.io.js"></script>" index.html
sudo sed -i "36ivar socket = io.connect('http://$pubip:4000');" index.html
cd ..
cd ..
cd ..
sudo node server.js

