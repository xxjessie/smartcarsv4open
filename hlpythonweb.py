from splinter import Browser  
from time import sleep  
import sys              
browser = Browser('phantomjs')
url = "http://127.0.0.1:8000/clientapp/"
browser.visit(url)
browser.find_by_id('demo_name').fill(sys.argv[1])
browser.find_by_tag('Button')[1].click()
browser.find_link_by_text('Request').click()
browser.find_link_by_text('Select Points').click()
dist = 0.001;
#start_lat = 40.7607;
#start_lng = -73.980;
start_lat = float(sys.argv[2]);
start_lng = float(sys.argv[3]);
for i in range(0,5):
	lat = start_lat + i*dist;
	lng = start_lng + i*dist;	
	browser.find_by_id('demo_lat').fill(str(lat))
	browser.find_by_id('demo_lng').fill(str(lng))
	browser.find_by_tag('Button').click()
browser.find_link_by_text('Stop').click()
browser.find_link_by_text('Start').click()


sleep(3000)
